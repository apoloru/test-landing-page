<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRegistroTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users_registro';

    /**
     * Run the migrations.
     * @table users_registro
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idusers_registro');
            $table->string('nombre')->nullable()->default(null);
            $table->string('apellido')->nullable()->default(null);
            $table->integer('identificacion')->nullable()->default(null);
            $table->integer('id_departamento')->nullable()->default(null);
            $table->integer('id_ciudad')->nullable()->default(null);
            $table->integer('celular')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->tinyInteger('autoriza')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
