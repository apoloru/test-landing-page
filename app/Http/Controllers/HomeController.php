<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserGanador;
use App\Models\UserRegistro;

class HomeController extends Controller
{
    /**
     *  Landing page
     **/
    public function index()
    {
    	$ganadores = UserGanador::
    		join('users_registro','users_registro.idusers_registro','=','users_ganadores.idUserRegistro')
    		->get();

        $registrados = UserRegistro::all();

    	return view('home.index', compact('ganadores','registrados'));
    }
}
