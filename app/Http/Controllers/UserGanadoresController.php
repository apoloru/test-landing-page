<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserGanador;
use App\Models\UserRegistro;

class UserGanadoresController extends Controller
{
    /**
     *	Sortear premio
     **/
    public function sorteo(Request $request){
    	$sorteo = UserRegistro::get();
    	if($sorteo->count() >= 5)
    	{
    		$maximo = UserRegistro::max('idusers_registro');
	    	$minimo = UserRegistro::min('idusers_registro');
	    	$codigoSorteo = mt_rand($minimo, $maximo);
	    	$ganador = UserRegistro::where('idusers_registro',$codigoSorteo)->first();
	    	$registrarGanador = UserGanador::create(["idUserRegistro"=> $ganador->idusers_registro]);

	    	return redirect()
                    ->back()
                    ->withInput();
        }else{
        	return "No estan como minimo los 5 participantes.";
        }
    }
}
