<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\UserRegistro;
use Illuminate\Support\Facades\Validator;

class UserRegistroController extends Controller
{
    /**
    * Registro de usuarios para participar en la rifa
    **/
    public function registroUser(Request $request/**, Requests\UserRegistroValidate $validate**/)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|min:2|max:50',
            'apellido' => 'required|string|min:2|max:50',
            'identificacion' => 'required|numeric|unique:users_registro,identificacion',
            'id_departamento' => 'required|numeric|min:1|max:50',
            'id_ciudad' => 'required|numeric|min:1|max:50',
            'celular' => 'required|numeric|unique:users_registro,celular',
            'email' => 'required|email|max:255|unique:users_registro,email',
            'autoriza' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput($request->input());
        }

    	$registro = UserRegistro::create($request->all());

    	if ($registro !== null) {
    		return back()->withInput();//response()->json("success" => true);
    	}else{
    		return back()->withInput();// response()->json("success" => false);
    	}
    }
}
