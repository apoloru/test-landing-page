<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentosArchivoPlanoController extends Controller
{
    /**
     *  Descargar documento registrados
     **/
    public function usersRegistrados()
    {
    	return datatables()->of(UserRegistro::query())->toJson();
    }

    /**
     *	Descargar documento ganadores
     **/
    public function usersGanadores()
    {
    	return datatables()->of(UserRegistro::query())->toJson();
    }
}
