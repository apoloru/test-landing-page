<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegistroValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomber' => 'required|string|min:2|max:50',
            'apellido' => 'required|string|min:2|max:50',
            'identificacion' => 'required|numeric|min:2|max:50|unique:users_registro,identificacion',
            'id_departamento' => 'required|numeric|min:2|max:50',
            'id_ciudad' => 'required|numeric|min:2|max:50',
            'celular' => 'required|numeric|min:2|max:50||unique:users_registro,celular',
            'email' => 'required|email|max:255|unique:users_registro,email',
            'autoriza' => 'required|numeric|min:1',
        ];
    }
}
