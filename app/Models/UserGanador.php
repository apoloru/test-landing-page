<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGanador extends Model
{
    protected $table    = 'users_ganadores';
    protected $fillable = [
    	'idusers_ganadores',
    	'idUserRegistro',
    	'created_at',
    	'updated_at'
    ];
}
