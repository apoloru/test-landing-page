<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRegistro extends Model
{
    protected $table    = 'users_registro';
    protected $fillable = [
    	'idusers_registro',
    	'nombre',
    	'apellido',
    	'identificacion',
    	'id_departamento',
    	'id_ciudad',
    	'celular',
    	'email',
    	'autoriza',
    	'created_at',
    	'updated_at'
    ];
}
