<!-- footer -->
<footer id="contacto">
    <div class="agileits-w3layouts-footer">
        <div class="container">
            <div class="col-md-4 w3-agile-grid">
                <h5>
                    Nosotros
                </h5>
                <p>
                    El Grupo Renault es una compañía internacional con raíces francesas y una historia que se remonta a más de 115 años atrás. Con presencia en 128 países, diseñamos, fabricamos y vendemos vehículos personales y comerciales bajo tres marcas: Renault, Dacia y RSM. Gracias a nuestra exclusiva alianza con Nissan y nuestras asociaciones estratégicas, somos el cuarto mayor fabricante de automóviles en todo el mundo. Hoy en día, contamos con más de 120.000 empleados en todo el mundo, todos impulsados por una pasión automotriz.
                </p>
                <div class="footer-agileinfo-social">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/inxait.corp" target="_black">
                                <i class="fa fa-facebook">
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/inxaitcorp?lang=es" target="_black">
                                <i class="fa fa-twitter">
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_black">
                                <i class="fa fa-rss">
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_black">
                                <i class="fa fa-vk">
                                </i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 w3-agile-grid">
                <h5>
                    Dirección
                </h5>
                <div class="w3-address">
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i aria-hidden="true" class="fa fa-phone">
                            </i>
                        </div>
                        <div class="w3-address-right">
                            <h6>
                                Número teléfonico
                            </h6>
                            <p>
                                +0(57) 320 226 0922
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i aria-hidden="true" class="fa fa-envelope">
                            </i>
                        </div>
                        <div class="w3-address-right">
                            <h6>
                                Dirección E-mail
                            </h6>
                            <p>
                                Email :
                                <a href="mailto:apolorubiano@gmail.com">
                                    apolorubiano@gmail.com
                                </a>
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i aria-hidden="true" class="fa fa-map-marker">
                            </i>
                        </div>
                        <div class="w3-address-right">
                            <h6>
                                Dirección
                            </h6>
                            <p>
                                Cr 65 # 78a-30 Colombia Bogotá D.C
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 w3-agile-grid">
                <h5>
                    Mensajes recientes
                </h5>
                <div class="w3ls-post-grids">
                    <div class="w3ls-post-grid">
                        <div class="w3ls-post-img">
                            <a href="#">
                                <img alt="" src="{{ url('images/p1.jpg') }}"/>
                            </a>
                        </div>
                        <div class="w3ls-post-info">
                            <h6>
                                <a data-target="#myModal" data-toggle="modal" href="#">
                                    Camionero 1
                                </a>
                            </h6>
                            <p>
                                Julio 10,2019
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="w3ls-post-grid">
                        <div class="w3ls-post-img">
                            <a href="#">
                                <img alt="" src="{{ url('images/p2.jpg') }}"/>
                            </a>
                        </div>
                        <div class="w3ls-post-info">
                            <h6>
                                <a data-target="#myModal" data-toggle="modal" href="#">
                                    Polo Rubiano
                                </a>
                            </h6>
                            <p>
                                Julio 17,2019
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="w3ls-post-grid">
                        <div class="w3ls-post-img">
                            <a href="#">
                                <img alt="" src="{{ url('images/p3.jpg') }}"/>
                            </a>
                        </div>
                        <div class="w3ls-post-info">
                            <h6>
                                <a data-target="#myModal" data-toggle="modal" href="#">
                                    Andrés Polo
                                </a>
                            </h6>
                            <p>
                                Junio 26,2019
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="w3ls-post-grid">
                        <div class="w3ls-post-img">
                            <a href="#">
                                <img alt="" src="{{ url('images/p1.jpg') }}"/>
                            </a>
                        </div>
                        <div class="w3ls-post-info">
                            <h6>
                                <a data-target="#myModal" data-toggle="modal" href="#">
                                    Arnulfo Rubiano
                                </a>
                            </h6>
                            <p>
                                Junio 28,2019
                            </p>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p>
                © 2019 INXAIT. Todos los derechos reservados | Realizado
                <a href="https://apoloru.com">
                    APOLORU
                </a>
            </p>
        </div>
    </div>
</footer>
<!-- //footer -->
