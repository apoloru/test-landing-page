<!DOCTYPE html>
<html lang="es">
    <head>
        <title>
            INXAIT
        </title>
        <!-- Meta tag Keywords -->
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="Rifa carro, sorteo carro, rifa auto, sorteo auto, gana auto, gana carro" name="keywords"/>
        <script type="application/x-javascript">
            addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); }
        </script>
        <!--// Meta tag Keywords -->
        <link href="{{ url('css/flexslider.css') }}" media="all" rel="stylesheet" type="text/css"/>
        <!-- for testimonials -->
        <!-- css files -->
        <link href="{{ url('css/time.css') }}" rel="stylesheet"/>
        <!-- Bootstrap-Core-CSS -->
        <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet"/>
        <!-- Bootstrap-Core-CSS -->
        <link href="{{ url('css/style.css') }}" media="all" rel="stylesheet" type="text/css"/>
        <!-- Style-CSS -->
        <link href="{{ url('css/font-awesome.css') }}" rel="stylesheet"/>
        <!-- Font-Awesome-Icons-CSS -->
        <!-- //css files -->
        <!-- web-fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&subset=latin-ext" rel="stylesheet"/>
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"/>
        <!-- //web-fonts -->
    </head>
    <body>
        <div class="header">
            @include('layout.nav')
        </div>
        @yield('contenido')
	@include('layout.footer')
        <!-- js-scripts -->
        <!-- js -->
        <script src="{{ url('js/jquery-2.1.4.min.js') }}" type="text/javascript">
        </script>
        <script src="{{ url('js/bootstrap.js') }}" type="text/javascript">
        </script>
        <!-- Necessary-JavaScript-File-For-Bootstrap -->
        <!-- //js -->
        <script src="{{ url('js/moment.js') }}">
        </script>
        <script src="{{ url('js/moment-timezone-with-data.js') }}">
        </script>
        <script src="{{ url('js/timer.js') }}">
        </script>
        <!-- start-smoth-scrolling -->
        <script src="{{ url('js/SmoothScroll.min.js') }}">
        </script>
        <script src="{{ url('js/move-top.js') }}" type="text/javascript">
        </script>
        <script src="{{ url('js/easing.js') }}" type="text/javascript">
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
        </script>
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
        </script>
        <!-- //here ends scrolling icon -->
        <!-- start-smoth-scrolling -->
        <!-- Baneer-js -->
        <script src="{{ url('js/responsiveslides.min.js') }}">
        </script>
        <script>
            $(function () {
			$("#slider").responsiveSlides({
				auto: true,
				pager:false,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
        </script>
        <!-- //Baneer-js -->
        <!-- banner bottom video script -->
        <script src="{{ url('js/simplePlayer.js') }}">
        </script>
        <script>
            $("document").ready(function() {
					$("#video").simplePlayer();
				});
        </script>
        <!-- //banner bottom video script -->
        <!-- Stats-Number-Scroller-Animation-JavaScript -->
        <script src="{{ url('js/waypoints.min.js') }}">
        </script>
        <script src="{{ url('js/counterup.min.js') }}">
        </script>
        <script>
            jQuery(document).ready(function( $ ) {
						$('.counter').counterUp({
							delay: 100,
							time: 1000
						});
					});
        </script>
        <!-- //Stats-Number-Scroller-Animation-JavaScript -->
        <!-- FlexSlider-JavaScript -->
        <script defer="" src="{{ url('js/jquery.flexslider.js') }}">
        </script>
        <script type="text/javascript">
            $(function(){
			$(".prueba").on("click", function(){
				
			})

			SyntaxHighlighter.all();
				});
				$(window).load(function(){
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
						$('body').removeClass('loading');
					}
			});
		});
        </script>
        <!-- //FlexSlider-JavaScript -->
        <!-- //js-scripts -->
    </body>
</html>