<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
            <span class="sr-only">
                Toggle navigation
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
        </button>
        <h1>
            <a href="index.html">
                INXAIT
            </a>
        </h1>
    </div>
    <div class="top-nav-text">
        <div class="nav-contact-w3ls">
            <i aria-hidden="true" class="fa fa-eye prueba">
            </i>
        </div>
    </div>
    <!-- navbar-header -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a class="hvr-underline-from-center active" href="index.html">
                    Inicio
                </a>
            </li>
            <li>
                <a class="hvr-underline-from-center scroll scroll" href="#sorteo">
                    Sorteo
                </a>
            </li>
            <li>
                <a class="hvr-underline-from-center scroll scroll" href="#noticias">
                    Noticias
                </a>
            </li>
            <li>
                <a class="hvr-underline-from-center scroll scroll" href="#contacto">
                    Contacto
                </a>
                @if($ganadores->count() > 0)
                <li>
                    <a class="hvr-underline-from-center scroll scroll" href="#ganadores">
                        Ganadores
                    </a>
                </li>
                @endif
            </li>
            @if($registrados->count() >= 5)
            <li>
                <a class="hvr-underline-from-center scroll scroll" href="{{ route('realizarSorteo') }}">
                    Sortear
                </a>
            </li>
            @endif
        </ul>
    </div>
    <div class="clearfix">
    </div>
</nav>