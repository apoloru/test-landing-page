@extends('layout.master')
@section('contenido')
<!-- Slider -->
<div class="slider">
    <div class="callbacks_container">
        <ul class="rslides" id="slider">
            <li>
                <div class="w3layouts-banner-top w3layouts-banner-top1">
                    <div class="banner-dott">
                        <div class="container">
                            <div class="slider-info">
                                <div class="col-md-8">
                                    <h2>
                                        !Dime quién eres y
                                    </h2>
                                    <h4>
                                        participa por un auto!
                                    </h4>
                                    <div class="w3ls-button">
                                        <a data-target="#myModal" data-toggle="modal" href="#">
                                            ¡Ponle nombre a tu Renault!
                                        </a>
                                    </div>
                                    <div class="bannergrids">
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-car">
                                            </i>
                                            <p>
                                                Maneja con estilo
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-wheelchair-alt">
                                            </i>
                                            <p>
                                                Conduce con total confort
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-thumbs-up">
                                            </i>
                                            <p>
                                                Siéntete seguro
                                            </p>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-xs">
                                        <div class="w3ls-button">
                                            <a data-target="#myModal" data-toggle="modal" href="#">
                                                Participar Rifa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3layouts-banner-top">
                    <div class="banner-dott">
                        <div class="container">
                            <div class="slider-info">
                                <div class="col-md-8">
                                    <h3>
                                        Un premio hecho
                                    </h3>
                                    <h4>
                                        a tu medida
                                    </h4>
                                    <div class="w3ls-button">
                                        <a data-target="#myModal" data-toggle="modal" href="#">
                                            ¡Ponle nombre a tu Renault!
                                        </a>
                                    </div>
                                    <div class="bannergrids">
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-car">
                                            </i>
                                            <p>
                                                Maneja con estilo
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-wheelchair-alt">
                                            </i>
                                            <p>
                                                Conduce con total confort
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-thumbs-up">
                                            </i>
                                            <p>
                                                Siéntete seguro
                                            </p>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-xs">
                                        <div class="w3ls-button">
                                            <a data-target="#myModal" data-toggle="modal" href="#">
                                                Participar Rifa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3layouts-banner-top w3layouts-banner-top3">
                    <div class="banner-dott">
                        <div class="container">
                            <div class="slider-info">
                                <div class="col-md-8">
                                    <h3>
                                        Gánate un Renault
                                    </h3>
                                    <h4>
                                        Stepwat AT 2019
                                    </h4>
                                    <div class="w3ls-button">
                                        <a data-target="#myModal" data-toggle="modal" href="#">
                                            ¡Ponle nombre a tu Renault!
                                        </a>
                                    </div>
                                    <div class="bannergrids">
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-car">
                                            </i>
                                            <p>
                                                Maneja con estilo
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-wheelchair-alt">
                                            </i>
                                            <p>
                                                Conduce con total confort
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-thumbs-up">
                                            </i>
                                            <p>
                                                Siéntete seguro
                                            </p>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-xs">
                                        <div class="w3ls-button">
                                            <a data-target="#myModal" data-toggle="modal" href="#">
                                                Participar Rifa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3layouts-banner-top w3layouts-banner-top2">
                    <div class="banner-dott">
                        <div class="container">
                            <div class="slider-info">
                                <div class="col-md-8">
                                    <h3>
                                        Te acercamos
                                    </h3>
                                    <h4>
                                        a lo que quieres
                                    </h4>
                                    <div class="w3ls-button">
                                        <a data-target="#myModal" data-toggle="modal" href="#">
                                            ¡Ponle nombre a tu Renault!
                                        </a>
                                    </div>
                                    <div class="bannergrids">
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-car">
                                            </i>
                                            <p>
                                                Maneja con estilo
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-wheelchair-alt">
                                            </i>
                                            <p>
                                                Conduce con total confort
                                            </p>
                                        </div>
                                        <div class="col-md-4 grid1">
                                            <i aria-hidden="true" class="fa fa-thumbs-up">
                                            </i>
                                            <p>
                                                Siéntete seguro
                                            </p>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="visible-xs">
                                        <div class="w3ls-button">
                                            <a data-target="#myModal" data-toggle="modal" href="#">
                                                Participar Rifa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="hidden-xs" style="position: absolute; z-index: 2; left: 50%; top:6%; width: 30%;">
        <!--<div class="col-md-8"></div>
		<div class="col-md-4">-->
        <div class="banner-form-agileinfo center-block" style="position:relative; left:40%;">
            <h5>
                Participa por este
                <span>
                    súper auto
                </span>
            </h5>
            <form action="{{ route('userRegistro') }}" method="post">
                @csrf
                <input class="nombre" name="nombre" placeholder="Nombre" required="" type="text" value="{{ old('nombre') }}"/>
                <input class="apellido" name="apellido" placeholder="Apellido" required="" type="text" value="{{ old('apellido') }}"/>
                <input class="identificacion" name="identificacion" placeholder="Cédula" required="" type="text" value="{{ old('identificacion') }}"/>
                <input class="celular" name="celular" placeholder="Número celular" required="" type="text" value="{{ old('celular') }}"/>
                <input class="email" name="email" placeholder="Dirección de correo" required="" type="text" value="{{ old('email') }}"/>
                <select class="form-control option-w3ls" name="id_departamento">
                    <option value="1">
                        Departamento
                    </option>
                    <option value="1">
                        Albania
                    </option>
                    <option value="1">
                        Belgium
                    </option>
                    <option value="1">
                        Cameroon
                    </option>
                    <option value="1">
                        Dominica
                    </option>
                    <option value="1">
                        France
                    </option>
                    <option value="1">
                        Jersey
                    </option>
                </select>
                <select class="form-control option-w3ls" name="id_ciudad">
                    <option value="1">
                        Ciudad
                    </option>
                    <option value="1">
                        Colombia
                    </option>
                    <option value="1">
                        Indonesia
                    </option>
                    <option value="1">
                        Japan
                    </option>
                    <option value="1">
                        Lebanon
                    </option>
                    <option value="1">
                        Luxembourg
                    </option>
                    <option value="1">
                        Montserrat
                    </option>
                </select>
                <input id="cbox2" name="autoriza" type="checkbox" value="1"/>
                <label for="cbox2">
                    <span>
                        “Autorizo el tratamiento de mis datos de acuerdo con la finalidad establecida en la política de protección de datos personales”.
                    </span>
                </label>
                <input class="hvr-shutter-in-vertical" type="submit" value="Participar">
                </input>
            </form>
        </div>
        <!--</div>
	</div>-->
        <div class="clearfix">
        </div>
    </div>
    <!-- //Slider -->
    <!-- bootstrap-modal-pop-up -->
    <div aria-labelledby="myModal" class="modal video-modal fade" id="myModal" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Transporters
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <img alt=" " class="img-responsive" src="{{ url('images/bg3.jpg') }}"/>
                    <p>
                        Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
                        <i>
                            " Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.
                        </i>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- //bootstrap-modal-pop-up -->
    <!-- banner-bottom -->
    <div class="banner-bottom" id="videopol">
        <div class="col-md-7 bannerbottomleft">
            <div class="video-grid-single-page-agileits">
                <div data-video="hIUZamyDZlw" id="video">
                    <img alt="" class="img-responsive" src="{{ url('images//bg2.jpg') }}"/>
                </div>
            </div>
        </div>
        <div class="col-md-5 bannerbottomright">
            <h3>
                ¿Cómo concursar?
            </h3>
            <p>
                Obtén el auto de tus sueños de forma sencilla.
            </p>
            <h4>
                <i aria-hidden="true" class="fa fa-taxi">
                </i>
                Registra todos los datos solicitados.
            </h4>
            <h4>
                <i aria-hidden="true" class="fa fa-shield">
                </i>
                Acepta el correo de confirmación de registro.
            </h4>
            <h4>
                <i aria-hidden="true" class="fa fa-ticket">
                </i>
                Espera los resultados del sorteo el 2019/08/01.
            </h4>
            <h4>
                <i aria-hidden="true" class="fa fa-space-shuttle">
                </i>
                Disfruta de tu nuevo auto.
            </h4>
        </div>
        <div class="clearfix">
        </div>
    </div>
    <!-- //banner-bottom -->
    <!-- team -->
    @if($ganadores->count() > 0)
    <div class="team" id="ganadores">
        <div class="container">
            <div class="heading">
                <h3>
                    Ganadores Sorteo 2019
                </h3>
            </div>
            <div class="wthree_team_grids">
                @foreach($ganadores as $count => $ganador)
                <div class="col-md-3 wthree_team_grid">
                    <div class="hovereffect">
                        <img alt=" " class="img-responsive" src="{{ url('images/team1.jpg') }}"/>
                        <div class="overlay">
                            <h6>
                                Ganador {{ ++$count }}
                            </h6>
                            <div class="rotate">
                                <p class="group1">
                                    <a href="#">
                                        <i class="fa fa-facebook">
                                        </i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <h4>
                        {{ $ganador->nombre }} {{ $ganador->apellido }}
                    </h4>
                    <p>
                        Registrado {{ $ganador->created_at }}
                    </p>
                    <span>
                        Número célular {{ $ganador->celular }}
                    </span>
                </div>
                @endforeach
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- //team -->
    <!-- Clients -->
    <div class=" col-md-6 clients" id="sorteo">
        <h3>
            Testimonials
        </h3>
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="client">
                            <img alt="" src="{{ url('images/t1.jpg') }}"/>
                            <h5>
                                Brian Fantana
                            </h5>
                            <div class="clearfix">
                            </div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .
                        </p>
                    </li>
                    <li>
                        <div class="client">
                            <img alt="" src="{{ url('images/t2.jpg') }}"/>
                            <h5>
                                Brick Tamland
                            </h5>
                            <div class="clearfix">
                            </div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .
                        </p>
                    </li>
                    <li>
                        <div class="client">
                            <img alt="" src="{{ url('images/t3.jpg') }}"/>
                            <h5>
                                Ron Burgundy
                            </h5>
                            <div class="clearfix">
                            </div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .
                        </p>
                    </li>
                    <li>
                        <div class="client">
                            <img alt="" src="{{ url('images/t4.jpg') }}"/>
                            <h5>
                                Arturo Mendez
                            </h5>
                            <div class="clearfix">
                            </div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .
                        </p>
                    </li>
                </ul>
            </div>
        </section>
    </div>
    <!-- //Clients -->
    <!-- Counter -->
    <div class="col-md-6 services-bottom">
        <div class="col-md-6 agileits_w3layouts_about_counter_left">
            <div class="countericon">
                <i aria-hidden="true" class="fa fa-calendar">
                </i>
            </div>
            <div class="counterinfo">
                <p class="counter timer w3" id="days">
                </p>
                <h3>
                    DÍAS
                </h3>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-6 agileits_w3layouts_about_counter_left">
            <div class="countericon">
                <i aria-hidden="true" class="fa fa-calendar">
                </i>
            </div>
            <div class="counterinfo">
                <p class="counter" id="hours">
                </p>
                <h3>
                    HORAS
                </h3>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="col-md-6 agileits_w3layouts_about_counter_left">
            <div class="countericon">
                <i aria-hidden="true" class="fa fa-calendar">
                </i>
            </div>
            <div class="counterinfo">
                <p class="counter" id="minutes">
                </p>
                <h3>
                    MINUTOS
                </h3>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-6 agileits_w3layouts_about_counter_left">
            <div class="countericon">
                <i aria-hidden="true" class="fa fa-calendar">
                </i>
            </div>
            <div class="counterinfo">
                <p class="counter" id="seconds">
                </p>
                <h3>
                    SEGUNDO
                </h3>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="clearfix">
        </div>
    </div>
    <div class="clearfix">
    </div>
    <!-- //Counter -->
    <!-- our blog -->
    <section class="blog" id="noticias">
        <div class="container">
            <div class="heading">
                <h3>
                    Últimas noticias
                </h3>
            </div>
            <div class="blog-grids">
                <div class="col-md-4 blog-grid">
                    <a data-target="#myModal" data-toggle="modal" href="#">
                        <img alt="" src="{{ url('images/bg4.jpg') }}"/>
                    </a>
                    <h5>
                        Julio 25,2019
                    </h5>
                    <h4>
                        <a data-target="#myModal" data-toggle="modal" href="#">
                            CAPTUR
                        </a>
                    </h4>
                    <p>
                        {{ str_limit('Déjate seducir por la atractiva silueta y la poderosa parrilla de Renault CAPTUR, Renault CAPTUR hace más fácil y práctica tu vida a bordo. Elige el confort: elige Renault CAPTUR.', 150) }}
                    </p>
                    <div class="readmore-w3">
                        <a class="readmore" data-target="#myModal" data-toggle="modal" href="#">
                            Leer más
                        </a>
                    </div>
                </div>
                <div class="col-md-4 blog-grid">
                    <a data-target="#myModal" data-toggle="modal" href="#">
                        <img alt="" src="{{ url('images/bg7.jpg') }}"/>
                    </a>
                    <h5>
                        Julio 26,2019
                    </h5>
                    <h4>
                        <a data-target="#myModal" data-toggle="modal" href="#">
                            ALASKAN
                        </a>
                    </h4>
                    <p>
                        {{ str_limit('La Renault ALASKAN integra en su ADN un diseño moderno e imponente que hace honor a sus capacidades. Cuenta con grandes y esculpidos guardabarros, una robusta e imponente parrilla cromada resaltando la nueva identidad de marca.', 150) }}
                    </p>
                    <div class="readmore-w3">
                        <a class="readmore" data-target="#myModal" data-toggle="modal" href="#">
                            Leer más
                        </a>
                    </div>
                </div>
                <div class="col-md-4 blog-grid">
                    <a data-target="#myModal" data-toggle="modal" href="#">
                        <img alt="" src="{{ url('images/bg8.jpg') }}"/>
                    </a>
                    <h5>
                        Julio 26,2019
                    </h5>
                    <h4>
                        <a data-target="#myModal" data-toggle="modal" href="#">
                            TWIZY
                        </a>
                    </h4>
                    <p>
                        {{ str_limit('TWIZY reinventa tu vida en la ciudad. Su diseño completamente innovador y eco-friendly te permite desplazarte exento de Pico y Placa se encarga del resto. Sus sistemas de asistencia hacen mucho más fácil la vida en el camino..', 150) }}
                    </p>
                    <div class="readmore-w3">
                        <a class="readmore" data-target="#myModal" data-toggle="modal" href="#">
                            Leer más
                        </a>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </section>
    <div class="alert alert-danger" id="errors" role="alert" style="position: absolute; z-index: 3; top:5%; left: 50%;">
        <h4 class="alert-heading">
            Problemas al registra!
        </h4>
        <ul>
            @foreach($errors->all() as $message)
            <li>
                {{ $message  }}
            </li>
            @endforeach
        </ul>
    </div>
    <script type="text/javascript">
        document.getElementById("errors").style.display = "none"; 

	@if($errors->all())
		document.getElementById("errors").style.display = "block"; 
	@endif
    </script>
    <!-- //our blog -->
    @endsection
</div>