<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomeController@index')->name('home');
Route::post('registro-usuarios-rifa','UserRegistroController@registroUser')->name('userRegistro');

/**
 *	Realizar sorteo con los participantes actuales
 **/
Route::get('realizar-sorteo','UserGanadoresController@sorteo')->name('realizarSorteo');